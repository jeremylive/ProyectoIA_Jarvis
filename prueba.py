import os
import math
import numpy as np
import tensorflow.compat.v2 as tf
import tensorflow_hub as hub

class progra:
	def __init__(self):
		self.pred=[]
		self.prob=[]
		self.list_porcent=[0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0]					    

	def loadImg(self,path_img):
		img=tf.io.read_file(path_img)
		img=tf.image.decode_jpeg(img)
		img=tf.image.convert_image_dtype(img,dtype=tf.float32,saturate=False)
		img=tf.expand_dims(img, axis=0)
		img=tf.image.resize_with_pad(img,513,513)

		model="https://hub.tensorflow.google.cn/google/seefood/segmenter/mobile_food_segmenter_V1/1"

		p_pred=hub.KerasLayer(model,output_key="food_group_segmenter:semantic_predictions")
		p_prob=hub.KerasLayer(model,output_key="food_group_segmenter:semantic_probabilities")		
		self.pred=p_pred(img).numpy()
		self.prob=p_prob(img).numpy()

	def getAperace(self, pred):
		porcent_list=[0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0]
		for tensor_part in pred:
			for tensor_line in tensor_part:
				for pixel in tensor_line:
					if pixel==0:
						porcent_list[0] += 1
					elif pixel==1:
						porcent_list[1] += 1
					elif pixel==2:
						porcent_list[2] += 1
					elif pixel==3:
						porcent_list[3] += 1
					elif pixel==4:
						porcent_list[4] += 1
					elif pixel==5:
						porcent_list[5] += 1
					elif pixel==6:
						porcent_list[6] += 1
					elif pixel==7:
						porcent_list[7] += 1
					elif pixel==8:
						porcent_list[8] += 1
					elif pixel==9:
						porcent_list[9] += 1
					elif pixel==10:
						porcent_list[10] += 1
					elif pixel==11:
						porcent_list[11] += 1
					elif pixel==12:
						porcent_list[12] += 1
					elif pixel==13:
						porcent_list[13] += 1
					elif pixel==14:
						porcent_list[14] += 1
					elif pixel==15:
						porcent_list[15] += 1
					elif pixel==16:
						porcent_list[16] += 1
					elif pixel==17:
						porcent_list[17] += 1
					elif pixel==18:
						porcent_list[18] += 1
					elif pixel==19:
						porcent_list[19] += 1
					elif pixel==20:
						porcent_list[20] += 1
					elif pixel==21:
						porcent_list[21] += 1
					elif pixel==22:
						porcent_list[22] += 1
					elif pixel==23:
						porcent_list[23] += 1
					elif pixel==24:
						porcent_list[24] += 1
					elif pixel==25:
						porcent_list[25] += 1
		return porcent_list

	def getPorcent(self,list):
		test_line=self.pred[0]
		total_line=len(test_line[0])
		total_colum=len(test_line)
		total=total_colum*total_line
		cont=0
		result=[]
		for i in list:
			porcentaje=round((i/total)*100)
			result.append(porcentaje)
			#print("Categoria:"+str(cont)+" -> Contador:"+str(i)+" -> Porcentaje:"+str(porcentaje))
			cont+=1
		#print("Total de Columnas:"+str(total_colum)+" -> "+"Total de filas:"+str(total_line)+" -> Total:"+str(total))
		self.list_porcent = result
		return result

	def getListPrice(self, porcent_list):
		pixel=0
		total_salad=0
		total_carne=0
		total_granos=0
		otros=0
		for porcent in porcent_list:
			if pixel==0:
				otros+=0
			elif pixel==1:
				total_salad+=porcent
			elif pixel==2:
				total_salad+=porcent
			elif pixel==3:
				total_salad+=porcent
			elif pixel==4:
				total_salad+=porcent
			elif pixel==5:
				if porcent > 15:
					otros+=150
			elif pixel==6:
				total_carne+=porcent
			elif pixel==7:
				total_carne+=porcent
			elif pixel==8:
				total_carne+=porcent
			elif pixel==9:
				total_carne+=porcent
			elif pixel==10:
				total_carne+=porcent
			elif pixel==11:
				total_granos+=porcent
			elif pixel==12:
				total_granos+=porcent
			elif pixel==13:
				total_granos+=porcent
			elif pixel==14:
				total_granos+=porcent
			elif pixel==15:
				total_granos+=porcent
			elif pixel==16:
				if porcent > 15:
					otros+=150
			elif pixel==17:
				if porcent > 15:
					otros+=50
			elif pixel==18:
				pass
			elif pixel==19:
				if porcent > 10:
					otros+=100
			elif pixel==20:
				if porcent > 20:
					otros+=250
			elif pixel==21:
				if porcent > 10:
					otros+=200
			elif pixel==22:
				if porcent > 4:
					otros+=50
			elif pixel==23:
				pass
			elif pixel==24:
				pass
			elif pixel==25:
				if porcent > 10:
					otros+=50
			pixel+=1
		flag1=0
		flag2=0
		flag3=0
		flag4=0
		if total_carne>=6 and total_carne<=12:
			flag1=1
		if total_salad>=15 and total_salad<=35:
			flag2=1
		if total_carne > 12 or total_salad > 35 or total_granos > 56:
			flag4 = 1
		#Balanceado
		if flag1==1 and flag2==1:
			return [total_carne,total_granos,total_salad,otros,1, flag4]

		flag1=0
		flag2=0
		if total_carne>=2 and total_carne<=5:
			flag1=1
		if total_salad>=8 and total_salad<=14:
			flag2=1
		if total_granos>=45 and total_granos<=56:
			flag3=1
		#Balanceado pero en baja calidad
		if (flag1==0 or flag2==1) or (flag1==1 or flag2==0) and flag3==1:
			return [total_carne,total_granos,total_salad,otros,0, flag4]
		#No balanceado
		return [total_carne,total_granos,total_salad,otros,-1, flag4]

	def getTotal(self, x):
		result=[]
		total=0
		x1=x[0]
		x2=x[1]
		x3=x[2]
		x4=x[3]
		x5=x[4]
		balanceFlag = 0

		if x4 == 1:
			balanceFlag = 2
			total = 1100+400+200
		elif x4==0:
			balanceFlag = 0
			total = 1500
		else:
			if x5 == 0:
				balanceFlag = 1
				total = 750
			else:
				balanceFlag = -1
				total = 3000

		result.append(total)
		result.append(balanceFlag)
		result.append(x1)
		result.append(x2)
		result.append(x3)
		return result

	def foodCost(self, total, balance, protein, carbohydrates, salad):
		message = ""
		hashtable = {1: "leafy greens", 2: "stem vegetables", 3: "non starchy roots", 4: "other vegetables",
			5: "fuits", 6: "meat", 7: "poultry", 8: "seafood", 9:"eggs", 10: "beans", 11: "baked goods", 
			12: "grains", 13: "pasta", 14: "starchy vegetables", 15: "other grains",
			16: "soups", 17: "herbs", 18: "dairy", 19: "snacks", 20: "desserts", 
			21: "beverages", 22: "sauces", 23: "food containers", 24: "dining tools", 25: "other food"}

		message += "\nThe total price of this food is " + str(total) + " colones.\n"
		if balance == 1:
			message += "It is healthy and balanced. "
		elif balance == 0:
			message += "It is healthy but low quality. "
		elif balance == -1:
			message += "It is not healthy. "



		message += "\nThe general porcents of the plate are "+str(protein)+" porcent of protein, "+str(carbohydrates)+" porcent of carbohydrates, "+str(salad)+" porcent of vegetables.\n"

		message += "The other porcentages corresponding to the sub categories are:\n"

		for i in range(1,25):
			if self.list_porcent[i] != 0 and i != 25:

				if i >= 6 and i <= 10:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " porcent of protein,\n"

				elif i >= 1 and i <= 4:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " porcent of vegetables,\n"

				elif i >= 11 and i <= 15:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " porcent of carbohydrates,\n"

		return message


img="pastas.jpeg"
classImg=progra()
classImg.loadImg(img)

x = classImg.getListPrice(classImg.getPorcent(classImg.getAperace(classImg.pred)))

result = classImg.getTotal(x)

z = classImg.foodCost(result[0], result[1], result[2], result[3], result[4])

print(z)




























# print(img)
# print(str(total))



#print(tf.argmax(classImg.prob,axis=0))
#classImg.workProb()






	# def workProb(self):

	# 	x=len(self.prob[0])
	# 	y=len(self.prob[0][0])

	# 	print(self.prob)

	# 	print("Tensor Probabilidad---> X:"+str(x)+" -> Y:"+str(y))

	# 	# for tensor_part in self.prob:
	# 	# 	for tensor_line in tensor_part:
	# 	# 		for part in tensor_line:
	# 	# 			for line in part:








		#print(pred.ndim) #3
		#print(prob.ndim) #4

		#print(pred.numpy())
		#print(prob.numpy()) #12 grupos
		
		#print(self.pred)










	#print(pred)            

	#print(np.array(pred))

	#print(prob)            
	#print(np.array(prob))









	# input_word_ids = tf.keras.layers.Input(
	#   shape=(None,), dtype=tf.int32, name='input_word_ids')
	# input_mask = tf.keras.layers.Input(
	#   shape=(None,), dtype=tf.int32, name='input_mask')
	# input_type_ids = tf.keras.layers.Input(
	#   shape=(None,), dtype=tf.int32, name='input_type_ids')

	# pooled_output, sequence_output = p_pred(
 #    [input_word_ids, input_mask, input_type_ids])

	# print(pooled_output)
	# print(sequence_output)








#import numpy as np
#import opencv as cv



	# model = tf.keras.models.load_model(
    #     model, custom_objects={"KerasLayer": hub.KerasLayer}
    # )
    #model.summary()

    #result = model.predict(df["sentence"].values)