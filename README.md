# ProyectoIA_Jarvis
Progra 1 IA



# Windows comandos de instalacion API REST FLASK
# Info detallada de los pasos a seguir
# https://code.visualstudio.com/docs/python/tutorial-flask

# No hacer este comando porque solo se hace cuando se creo el proyecto

python -m venv env

# Hacer estos comandos de fijo porque se necesita estas librerias

python -m pip install --upgrade pip

python -m pip install flask

# Ejecutar la API REST FLASK

python -m flask run

# Correr en el navegador la APP

http://127.0.0.1:5000/

# Utilizar las dos teclas CTRL+C para salir de la ejecución

#
# ProyectoIA_Jarvis
#
# ##
# sudo apt-get install python3-venv
# python3 -m venv env
# python3 -m pip install flask
# ##
# pip install matplotlib
# pip install seaborn
# pip install pickle5
# pip install sklearn
# pip install scikit-learn==0.22.2.post1 
# ##
# python3 -m flask run
# ##
# sudo apt-get install python3-pyaudio
# pip install SpeechRecognition

# ## IA
# pip install opencv-python
# pip install azure-cognitiveservices-vision-face
# pip install statsmodels