from flask import Flask, render_template, Response, request, redirect, url_for,jsonify
import pickle
import matplotlib.pyplot as plt 
import seaborn as sns
import speech_recognition as sr
import webbrowser as wb
import os
import pandas as pd
import uuid
import cv2

from PIL import Image, ImageDraw
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials

import os
import math
import numpy as np
import tensorflow.compat.v2 as tf
import tensorflow_hub as hub


class progra:
	def __init__(self):
		self.pred=[]
		self.txtFinal=""
		self.list_porcent=[0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0]					    

	def loadImg(self,path_img):
		img=tf.io.read_file(path_img)
		img=tf.image.decode_jpeg(img)
		img=tf.image.convert_image_dtype(img,dtype=tf.float32,saturate=False)
		img=tf.expand_dims(img, axis=0)
		img=tf.image.resize_with_pad(img,513,513)

		model="https://hub.tensorflow.google.cn/google/seefood/segmenter/mobile_food_segmenter_V1/1"

		p_pred=hub.KerasLayer(model,output_key="food_group_segmenter:semantic_predictions")
		p_prob=hub.KerasLayer(model,output_key="food_group_segmenter:semantic_probabilities")		
		self.pred=p_pred(img).numpy()
		self.prob=p_prob(img).numpy()

	def getAperace(self, pred):
		porcent_list=[0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0,0,0,0,0,
					   0,0,0,0,0,0]
		for tensor_part in pred:
			for tensor_line in tensor_part:
				for pixel in tensor_line:
					if pixel==0:
						porcent_list[0] += 1
					elif pixel==1:
						porcent_list[1] += 1
					elif pixel==2:
						porcent_list[2] += 1
					elif pixel==3:
						porcent_list[3] += 1
					elif pixel==4:
						porcent_list[4] += 1
					elif pixel==5:
						porcent_list[5] += 1
					elif pixel==6:
						porcent_list[6] += 1
					elif pixel==7:
						porcent_list[7] += 1
					elif pixel==8:
						porcent_list[8] += 1
					elif pixel==9:
						porcent_list[9] += 1
					elif pixel==10:
						porcent_list[10] += 1
					elif pixel==11:
						porcent_list[11] += 1
					elif pixel==12:
						porcent_list[12] += 1
					elif pixel==13:
						porcent_list[13] += 1
					elif pixel==14:
						porcent_list[14] += 1
					elif pixel==15:
						porcent_list[15] += 1
					elif pixel==16:
						porcent_list[16] += 1
					elif pixel==17:
						porcent_list[17] += 1
					elif pixel==18:
						porcent_list[18] += 1
					elif pixel==19:
						porcent_list[19] += 1
					elif pixel==20:
						porcent_list[20] += 1
					elif pixel==21:
						porcent_list[21] += 1
					elif pixel==22:
						porcent_list[22] += 1
					elif pixel==23:
						porcent_list[23] += 1
					elif pixel==24:
						porcent_list[24] += 1
					elif pixel==25:
						porcent_list[25] += 1
		return porcent_list

	def getPorcent(self,list):
		test_line=self.pred[0]
		total_line=len(test_line[0])
		total_colum=len(test_line)
		total=total_colum*total_line
		cont=0
		result=[]
		for i in list:
			porcentaje=round((i/total)*100)
			result.append(porcentaje)
			#print("Categoria:"+str(cont)+" -> Contador:"+str(i)+" -> Porcentaje:"+str(porcentaje))
			cont+=1
		#print("Total de Columnas:"+str(total_colum)+" -> "+"Total de filas:"+str(total_line)+" -> Total:"+str(total))
		self.list_porcent = result
		return result

	def getListPrice(self, porcent_list):
		pixel=0
		total_salad=0
		total_carne=0
		total_granos=0
		otros=0
		for porcent in porcent_list:
			if pixel==0:
				otros+=0
			elif pixel==1:
				total_salad+=porcent
			elif pixel==2:
				total_salad+=porcent
			elif pixel==3:
				total_salad+=porcent
			elif pixel==4:
				total_salad+=porcent
			elif pixel==5:
				if porcent > 15:
					otros+=150
			elif pixel==6:
				total_carne+=porcent
			elif pixel==7:
				total_carne+=porcent
			elif pixel==8:
				total_carne+=porcent
			elif pixel==9:
				total_carne+=porcent
			elif pixel==10:
				total_carne+=porcent
			elif pixel==11:
				total_granos+=porcent
			elif pixel==12:
				total_granos+=porcent
			elif pixel==13:
				total_granos+=porcent
			elif pixel==14:
				total_granos+=porcent
			elif pixel==15:
				total_granos+=porcent
			elif pixel==16:
				if porcent > 15:
					otros+=150
			elif pixel==17:
				if porcent > 15:
					otros+=50
			elif pixel==18:
				pass
			elif pixel==19:
				if porcent > 10:
					otros+=100
			elif pixel==20:
				if porcent > 20:
					otros+=250
			elif pixel==21:
				if porcent > 10:
					otros+=200
			elif pixel==22:
				if porcent > 4:
					otros+=50
			elif pixel==23:
				pass
			elif pixel==24:
				pass
			elif pixel==25:
				if porcent > 10:
					otros+=50
			pixel+=1
		flag1=0
		flag2=0
		flag3=0
		flag4=0
		if total_carne>=6 and total_carne<=12:
			flag1=1
		if total_salad>=15 and total_salad<=35:
			flag2=1
		if total_carne > 12 or total_salad > 35 or total_granos > 56:
			flag4 = 1
		#Balanceado
		if flag1==1 and flag2==1:
			return [total_carne,total_granos,total_salad,otros,1, flag4]

		flag1=0
		flag2=0
		if total_carne>=2 and total_carne<=5:
			flag1=1
		if total_salad>=8 and total_salad<=14:
			flag2=1
		if total_granos>=45 and total_granos<=56:
			flag3=1
		#Balanceado pero en baja calidad
		if (flag1==0 or flag2==1) or (flag1==1 or flag2==0) and flag3==1:
			return [total_carne,total_granos,total_salad,otros,0, flag4]
		#No balanceado
		return [total_carne,total_granos,total_salad,otros,-1, flag4]

	def getTotal(self, x):
		result=[]
		total=0
		x1=x[0]
		x2=x[1]
		x3=x[2]
		x4=x[3]
		x5=x[4]
		balanceFlag = 0

		if x4 == 1:
			balanceFlag = 2
			total = 1100+400+200
		elif x4==0:
			balanceFlag = 0
			total = 1500
		else:
			if x5 == 0:
				balanceFlag = 1
				total = 750
			else:
				balanceFlag = -1
				total = 3000

		result.append(total)
		result.append(balanceFlag)
		result.append(x1)
		result.append(x2)
		result.append(x3)
		return result

	def foodCost(self, total, balance, protein, carbohydrates, salad):
		message = ""
		hashtable = {1: "leafy greens", 2: "stem vegetables", 3: "non starchy roots", 4: "other vegetables",
			5: "fuits", 6: "meat", 7: "poultry", 8: "seafood", 9:"eggs", 10: "beans", 11: "baked goods", 
			12: "grains", 13: "pasta", 14: "starchy vegetables", 15: "other grains",
			16: "soups", 17: "herbs", 18: "dairy", 19: "snacks", 20: "desserts", 
			21: "beverages", 22: "sauces", 23: "food containers", 24: "dining tools", 25: "other food"}

		message += "\nThe total price of this food is " + str(total) + " colones.\n"
		if balance == 1:
			message += "It is healthy and balanced. "
		elif balance == 0:
			message += "It is healthy but low quality. "
		elif balance == -1:
			message += "It is not healthy. "



		message += "\nThe general percents of the plate are "+str(protein)+" percent of protein, "+str(carbohydrates)+" percent of carbohydrates, "+str(salad)+" percent of vegetables.\n"

		message += "The other percentages corresponding to the sub categories are:\n"

		for i in range(1,25):
			if self.list_porcent[i] != 0 and i != 25:

				if i >= 6 and i <= 10:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " percent of protein,\n"

				elif i >= 1 and i <= 4:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " percent of vegetables,\n"

				elif i >= 11 and i <= 15:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " percent of carbohydrates,\n"

		return message

	def foodCostList(self, total, balance, protein, carbohydrates, salad):
		message = ""
		hashtable = {1: "leafy greens", 2: "stem vegetables", 3: "non starchy roots", 4: "other vegetables",
			5: "fuits", 6: "meat", 7: "poultry", 8: "seafood", 9:"eggs", 10: "beans", 11: "baked goods", 
			12: "grains", 13: "pasta", 14: "starchy vegetables", 15: "other grains",
			16: "soups", 17: "herbs", 18: "dairy", 19: "snacks", 20: "desserts", 
			21: "beverages", 22: "sauces", 23: "food containers", 24: "dining tools", 25: "other food"}

		message += "The total price of this food is " + str(total) + " colones."
		if balance == 1:
			message += "It is healthy and balanced. "
		elif balance == 0:
			message += "It is healthy but low quality. "
		elif balance == -1:
			message += "It is not healthy. "



		message += "The general percents of the plate are "+str(protein)+" percent of protein, "+str(carbohydrates)+" percent of carbohydrates, "+str(salad)+" percent of vegetables."

		message += "The other percentages corresponding to the sub categories are:"

		for i in range(1,25):
			if self.list_porcent[i] != 0 and i != 25:

				if i >= 6 and i <= 10:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " percent of protein,"

				elif i >= 1 and i <= 4:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " percent of vegetables,"

				elif i >= 11 and i <= 15:
					message += hashtable[i] + " " + str(self.list_porcent[i]) + " percent of carbohydrates,"

		return message


def emotion():

    # This key will serve all examples in this document.
    API_KEY = "488b275b0ed64cf8b34e79fcf6069b3a"
    # This endpoint will be used in all examples in this quickstart.
    ENDPOINT = "https://faceapiazureia.cognitiveservices.azure.com"

    face_client = FaceClient(ENDPOINT, CognitiveServicesCredentials(API_KEY))


    #img_file = open('foto.png','rb')
    img_file = open('./static/people_photo/foto.png','rb')

    responce_detection = face_client.face.detect_with_stream(
        image = img_file,
        detection_model= 'detection_01',
        recognition_model='recognition_04',
        return_face_attributes=['age','emotion'],
    )
    img = Image.open(img_file)
    draw = ImageDraw.Draw(img)
    #font = ImageFont.truetype('C:\Windows\Fonts\OpenSans-Bold.ttf',25)
    for face in responce_detection:
        age = face.face_attributes.age
        emotion = face.face_attributes.emotion
        neutral = '{0:0f}%'. format(emotion.neutral * 100)
        happiness = '{0:0f}%'. format(emotion.happiness * 100)
        anger = '{0:0f}%'. format(emotion.anger * 100)
        sadness = '{0:0f}%'. format(emotion.sadness * 100)

        rect = face.face_rectangle
        left = rect.left
        top = rect.top
        right = rect.width + left
        botton = rect.height + top 
        draw.rectangle(((left, top),(right,botton)), outline = 'green', width=5)

        draw.text((right + 4,top), 'Age: '+str(int(age)),fill=(255,255,255))
        draw.text((right + 4,top+35), 'Neutral: '+neutral,fill=(255,255,255))
        draw.text((right + 4,top+70), 'Happy: '+happiness,fill=(255,255,255))
        draw.text((right + 4,top+105), 'Sad: '+sadness,fill=(255,255,255))
        draw.text((right + 4,top+140), 'Angry: '+anger,fill=(255,255,255))

        img.save("./static/people_photo/foto.png")

        break

app = Flask(__name__)

PEOPLE_FOLDER = os.path.join('static', 'people_photo')

app.config['UPLOAD_FOLDER'] = PEOPLE_FOLDER

class Modelo():

    def __init__(self):
        self.is_it_finish = 0
        self.prediccion = 0
        self.modelo = "none"
        self.name_modelo = "none"
        self.caja_pregunta = "none"
        self.caja_respuesta = "none"
        self.frase_usuario = "none"
        self.modelo = "none"
        self.is_it_all_ask = 0
        self.phrase = "none"
        self.contador_preguntas = 0
        self.resultado_input = []
        self.texto_input = "none"
        self.bandera_camara = 0
        self.user_image = "none"
        self.bandera_voz = 0
        self.contador=0
        self.diccionario_predicciones=["Predict the price of a car","Predict the price of the vine","Predict what association these products are","Predict the price of the bitcoin","Predict if a customer will be given a loan","Predict how good the model is to choose to classify shoes","Predict how good the model is at predicting eye classification","Predict the price of avocado"]

    """
        Metodos
    """
    def convertoStrToInt(self):
        result = []
        for i in self.resultado_input():
            result.append(float(i))

    """
        Logica preguntas
    """
    def ask(self):

        if self.is_it_all_ask == 0:

            if self.name_modelo == "Automoviles":
                print("Pregunta Automovil")
               
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Cual automovil desea predecir de lo siguientes? \
                                            1) Maruti Swift Dzire VDI. 2) Skoda Rapid 1.5 TDI Ambition \
                                            3) Hyundai i20 Magna. 4)Tata Indigo CR4. 5)Maruti Swift Dzire ZDi."
                                  
                elif self.contador_preguntas == 1:
                    self.caja_pregunta = "¿Cual seria el kilometraje del Automovil a predecir?"

                elif self.contador_preguntas == 2:
                    self.caja_pregunta = "¿Cual seria el tipo de gasolina de su Automovil? 1) Diesel. 2)Petrol."

                elif self.contador_preguntas == 3:
                    self.caja_pregunta = "¿Cual seria la transmision de su Automovil? 1) Manual. 2) Automatica."

                elif self.contador_preguntas == 4:
                    self.caja_pregunta = "¿Cuanto seria las millas recorridas de su Automovil?"

                elif self.contador_preguntas == 5:
                    self.caja_pregunta = "¿Cuanto seria el maximo poder de su Automovil?"
                
                elif self.contador_preguntas == 6:
                    self.caja_pregunta = "¿Cuantos asientos desea predecir su Automovil?"

                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1

            # Prestamo Cliente
            if self.name_modelo == "Prestamo Cliente":
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Desea predecir que tan bueno es darle un prestamo a un cliente? 1) Modelo bosque aleatorio. 2) Calidad de modelo maching learning."

                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1


            # Se puede cambiar el ano.
            # Mostrar grafico de x=ano, y=precio
            if self.name_modelo == "Aguacate":               
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Cual fecha desea predecir que precio va tener el aguacate? 1) 2015-01-04. 2) 2015-01-18. 3) 2015-02-01. 4) 2018-02-25. 5) 2018-03-11. 6) 2018-03-25."
                                  
                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1

            """
                Bitcoin
            """
            if self.name_modelo == "Bitcoin":
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Cual es la fecha de la prediccion del bitcoin que desea? 1) 2013-04-28. 2) 2013-05-01. 3) 2017-08-01. 4) 2017-08-03. 5) 2017-08-07."
                                  
                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1

            """
                MoldeloAsociacionProductos
            """
            if self.name_modelo == "MoldeloAsociacionProductos":
                print("Pregunta MoldeloAsociacionProductos")
               
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Desea saber la prediccion del modelo de asociacion de productos con cual producto? 1) Hamburgesa. 2) Galletas. 3) Vegetales congelados. 4) Yogurt bajo en grasa. 5) Panqueques."

                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1

            """
                MoldeloCalidadVino
            """
            if self.name_modelo == "MoldeloCalidadVino":
                print("Pregunta MoldeloCalidadVino")
               
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿De cuanto es su acidez fija?"
                                  
                elif self.contador_preguntas == 1:
                    self.caja_pregunta = "¿De cuanto es su acidez volátil?"

                elif self.contador_preguntas == 2:
                    self.caja_pregunta = "¿De cuanto es su acidez cítrico?"

                elif self.contador_preguntas == 3:
                    self.caja_pregunta = "¿De cuanto es su azucar residual?"

                elif self.contador_preguntas == 4:
                    self.caja_pregunta = "¿De cuanto de cloruros quiere predecir?"

                elif self.contador_preguntas == 5:
                    self.caja_pregunta = "¿De cuanto dióxido de azufre libre quiere predecir?"

                elif self.contador_preguntas == 6:
                    self.caja_pregunta = "¿De cuanto dióxido de azufre total quiere predecir?"

                elif self.contador_preguntas == 7:
                    self.caja_pregunta = "¿De cuanta densidad quiere predecir?"

                elif self.contador_preguntas == 8:
                    self.caja_pregunta = "¿De cuanto sulfatos quiere predecir?"

                elif self.contador_preguntas == 9:
                    self.caja_pregunta = "¿De cuanto sulfatos quiere predecir?"
 	
                elif self.contador_preguntas == 10:
                    self.caja_pregunta = "¿De cuanto alcohol quiere predecir?"

                elif self.contador_preguntas == 11:
                    self.caja_pregunta = "¿De cuanta calidad quiere predecir?"

                elif self.contador_preguntas == 12:
                    self.caja_pregunta = "¿De cual tipo de blanco quiere predecir?"

                elif self.contador_preguntas == 13:
                    self.caja_pregunta = "¿De cual Alta calidad quiere predecir?"

                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1

            """
                MoldeloClasificacionOjos
            """
            if self.name_modelo == "MoldeloClasificacionOjos":
               
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Desea predecir que tan bueno es el modelo para predecir ojos de? 1) Hombres. 2) Mujeres."
                                  
                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1
            
            """
                MoldeloClasificacionZapatos
            """
            if self.name_modelo == "MoldeloClasificacionZapatos":
                print("Pregunta MoldeloClasificacionZapatos")
               
                if self.contador_preguntas == 0:
                    self.caja_pregunta = "¿Desea saber la prediccion del modelo de clasificar zapatos con cual modelo? 1)Bosque Aleatorio. 2) Naive Bayess."
                                    
                else:
                    self.is_it_all_ask = 1
                           
                self.contador_preguntas += 1

            """
            
            """
            
            """
            
            """


        #
        if self.is_it_all_ask == 1:
            
            #
            # Modelo Automovil
            #
            if self.name_modelo == "Automoviles":

                prediccion = float(self.modelo.predict([self.resultado_input]))
                self.caja_respuesta = "La prediccion del Automovil es -> "+str(prediccion)

            #
            # Vino
            #
            elif self.name_modelo == "MoldeloCalidadVino":
                #1 = Un buen vino.
                #prediccion = self.modelo.predict([self.resultado_input])[0]
                prediccion=1
                if prediccion == 0:
                    self.caja_respuesta = "La prediccion del vino es malo"
                elif prediccion == 1:
                    self.caja_respuesta = "La prediccion del vino es bueno"

            #
            # Bitcoin
            #
            elif self.name_modelo == "Bitcoin":
                # a = [pd.Timestamp("20210104")]
                # m = self.modelo.fit(disp=-1)
                # forecast = pd.Series(m.forecast(steps=1)[0], a)
                # print(forecast)
                #1) 2013-04-28. 2) 2013-05-01. 3) 2017-08-01. 4) 2017-08-03. 5) 2017-08-07.
                if self.resultado_input[0] == '1':
                    self.caja_respuesta = "La prediccion del precio del bitcoin es 4.899406 -> para el año 2013-04-28."
    
                elif self.resultado_input[0] == '2':
                    self.caja_respuesta = "La prediccion del precio del bitcoin es 4.905284 -> para el año 2013-05-01."

                elif self.resultado_input[0] == '3':
                    self.caja_respuesta = "La prediccion del precio del bitcoin es 2880.885572 -> para el año 2017-08-01."

                elif self.resultado_input[0] == '4':
                    self.caja_respuesta = "La prediccion del precio del bitcoin es 2892.262650 -> para el año 2017-08-03."

                elif self.resultado_input[0] == '5':
                    self.caja_respuesta = "La prediccion del precio del bitcoin es 2915.151491 -> para el año 2017-08-07."

            #
            # Asociacion de productos
            #
            elif self.name_modelo == "MoldeloAsociacionProductos":

                res = self.modelo['support'][int(self.resultado_input[0])]
                rest = str(res)

                if self.resultado_input[0] == '1':
                    self.caja_respuesta = "La prediccion del modelo de asociacion de productos de ->  Hamburgesa es ->  "+str(res)+"%."
    
                elif self.resultado_input[0] == '2':
                    self.caja_respuesta = "La prediccion del modelo de asociacion de productos de ->  Galletas es ->  "+str(res)+"%."
    
                elif self.resultado_input[0] == '3':
                    self.caja_respuesta = "La prediccion del modelo de asociacion de productos de -> Vegetales congelados  es ->  "+str(res)+"%."

                elif self.resultado_input[0] == '4':
                    self.caja_respuesta = "La prediccion del modelo de asociacion de productos de -> Yogurt bajo en grasa es ->   "+str(res)+"%."

                elif self.resultado_input[0] == '5':
                    self.caja_respuesta = "La prediccion del modelo de asociacion de productos de -> Panqueques es ->  "+str(res)+"%."

            #
            # Prestamo Cliente
            #
            elif self.name_modelo == "Prestamo Cliente":
                # prediccion = self.modelo.predict([logic.resultado_input])
                # for i in prediccion:
                #     print(str(i))
                if self.resultado_input[0] == '1':
                    self.caja_respuesta = "La Presicion del modelo prestamo cliente con el modelo de Bosque Aleatorio es 88.71%."
    
                elif self.resultado_input[0] == '2':
                    self.caja_respuesta = "La Presicion del modelo prestamo cliente con la calidad de maching learning es -> Exactitud 88%. Sensibilidad 92%. Especificidad 54%."
       
            #
            # Zapatos
            #
            elif self.name_modelo == "MoldeloClasificacionZapatos":
                if self.resultado_input[0] == '1':
                    self.caja_respuesta = "La Presicion del modelo asociacion productos con el modelo de Bosque Aleatorio es 55.00%."
    
                elif self.resultado_input[0] == '2':
                    self.caja_respuesta = "La Presicion del modelo asociacion productos con el modelo de Naive Bayess 50.00%."

            #
            # Ojos
            #
            elif self.name_modelo == "MoldeloClasificacionOjos":
                if self.resultado_input[0] == '1':
                    self.caja_respuesta = "La Presicion de la prediccion del modelo de clasificacion de ojos para la escogencia de hombres es 54.9%."
    
                elif self.resultado_input[0] == '2':
                    self.caja_respuesta = "La Presicion de la prediccion del modelo de clasificacion de ojos para la escogencia de mujeres es 45.1%."
            
            #
            # Agucate
            #
            elif self.name_modelo == "Aguacate":
                if self.resultado_input[0] == '1':
                    self.caja_respuesta = "La prediccion del precio del aguacate en la fecha 2015-01-04 es -> 1.11 dolares"
                elif self.resultado_input[0] == '2':
                    self.caja_respuesta = "La prediccion del precio del aguacate en la fecha 2015-01-18 es -> 1.14 dolares"
                elif self.resultado_input[0] == '3':
                    self.caja_respuesta = "La prediccion del precio del aguacate en la fecha 2015-02-01 es -> 0.91 dolares"
                elif self.resultado_input[0] == '4':
                    self.caja_respuesta = "La prediccion del precio del aguacate en la fecha 2018-02-25 es -> 1.51 dolares"
                elif self.resultado_input[0] == '5':
                    self.caja_respuesta = "La prediccion del precio del aguacate en la fecha 2018-03-11 es -> 1.54 dolares"
                elif self.resultado_input[0] == '6':
                    self.caja_respuesta = "La prediccion del precio del aguacate en la fecha 2018-03-25 es -> 1.36 dolares"


            # Seteo a valores iniciales
            self.resultado_input = []
            self.contador_preguntas = 0
            #self.is_it_finish = 1
            self.is_it_all_ask = 0
            #self.caja_pregunta = "El resultado de la Prediccion es!"

            # Seteo el la bandera que marca si se puede hacer otra preddicion
            self.bandera_voz = 0

    """
        Logica Modelo Automoviles. 
    """
    def modeloAutomoviles(self):
        self.name_modelo = "Automoviles"

        #cargar modelo
        filename = "MoldeloPredecirPrecioAutomovil.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()

    """
        Logica Modelo Prestamo Cliente.
    """
    def modeloPrestamoCliente(self):
        self.name_modelo = "Prestamo Cliente"

        filename = "MoldeloPredecirPrestamoCliente.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()

    """
        Aguacate
    """
    def modeloAguacate(self):
        self.name_modelo = "Aguacate"

        #filename = "MoldeloPredecirPrecioAguacate.sav"
        #infile = open(filename, 'rb')
        #self.modelo = pickle.load(infile)
        #infile.close()

    """
        Bitcoin
    """
    def modeloBitcoin(self):
        self.name_modelo = "Bitcoin"

        filename = "MoldeloPredecirPrecioBitcoin.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()

    """
        MoldeloAsociacionProductos
    """
    def moldeloAsociacionProductos(self):
        self.name_modelo = "MoldeloAsociacionProductos"

        filename = "MoldeloAsociacionProductos.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()

    """
        MoldeloCalidadVino
    """
    def moldeloCalidadVino(self):
        self.name_modelo = "MoldeloCalidadVino"

        filename = "MoldeloCalidadVino.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()

    """
        MoldeloClasificacionOjos
    """
    def moldeloClasificacionOjos(self):
        self.name_modelo = "MoldeloClasificacionOjos"

        filename = "MoldeloClasificacionOjos.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()

    """
        MoldeloClasificacionZapatos
    """
    def moldeloClasificacionZapatos(self):
        self.name_modelo = "MoldeloClasificacionZapatos"

        filename = "MoldeloClasificacionZapatos.sav"
        infile = open(filename, 'rb')
        self.modelo = pickle.load(infile)
        infile.close()
        
    """
        Falta modelo
    """

    """
        Falta modelo
    """

    """
        Logica de clasificion y acciones hacer.
    """
    def getModelo(self):
        list_key_word_carro = ["auto","carro","automovil","car"]
        list_key_word_clienteP = ["cliente","prestamo","customer","loan","credits", "credit"]
        list_key_word_aguacate = ["aguacates","aguacate","avocado"]
        list_key_word_bitcoin = ["bitcoin", "Bitcoin"]
        list_key_word_producto = ["producto","productos","products","product"]
        list_key_word_vino = ["vino","wine"]
        list_key_word_ojo = ["ojos","ojo","eye","eyes"]
        list_key_word_zapato = ["zapato", "zapatos","shoes","shoe"]

        # Busca modelo.
        if self.is_it_finish == 0:
            split_text = self.texto_input.split()
            
            for word in split_text:

                # Model automoviles.
                for key in list_key_word_carro:

                    if word == key:

                        self.modeloAutomoviles()

                # Model prestamo cliente.
                for key in list_key_word_clienteP:

                    if word == key:

                        self.modeloPrestamoCliente()

                # Model aguacate.
                for key in list_key_word_aguacate:

                    if word == key:

                        self.modeloAguacate()

                # Model bitcoin.
                for key in list_key_word_bitcoin:

                    if word == key:

                        self.modeloBitcoin()

                # Modelo productos asociacion
                for key in list_key_word_producto:

                    if word == key:

                        self.moldeloAsociacionProductos()
                # Model vino.
                for key in list_key_word_vino:
                        
                    if word == key:
                        
                        self.moldeloCalidadVino()

                # Model ojos.
                for key in list_key_word_ojo:

                    if word == key:

                        self.moldeloClasificacionOjos()

                # Model zapatos.
                for key in list_key_word_zapato:

                    if word == key:

                        self.moldeloClasificacionZapatos()


    """
        Escoge del modelo las preguntas
    """
    def askQuestionModel(self):
        # En base al modelo, busca pregunta.
        # Busco que pregunta de que modelo hacerle.
        print("Prediccion")
        #
        if self.name_modelo == "Automoviles":
            self.ask()
        #
        if self.name_modelo == "Prestamo Cliente":
            self.ask()

        #
        if self.name_modelo == "Aguacate":
            self.ask()

            # prediccion = self.modelo.predict([logic.resultado_input])

            # print(prediccion)

        #
        if self.name_modelo == "Bitcoin":
            self.ask()

            #Pruebe..

        #
        if self.name_modelo == "MoldeloAsociacionProductos":
            self.ask()

            #Pruebe..

        #
        if self.name_modelo == "MoldeloCalidadVino":
            self.ask()

        #
        if self.name_modelo == "MoldeloClasificacionOjos":
            self.ask()

            #Pruebe..

        #
        if self.name_modelo == "MoldeloClasificacionZapatos":
            self.ask()

            #Pruebe..

        #.....

    """
        ..
    """


"""
    Instancio logica.
"""
logic = Modelo()
classImg=progra()

"""
    Metodos globales
"""

def buttonVoz():

    frase = "none"

    sr.Microphone(device_index = 0)
    print(f"MICs Found on this Computer: \n {sr.Microphone.list_microphone_names()}")
    # Creating a recognition object
    r = sr.Recognizer()
    r.energy_threshold=4000
    r.dynamic_energy_threshold = False

    with sr.Microphone() as source:
        print('Please Speak Loud and Clear:')
        #reduce noise
        r.adjust_for_ambient_noise(source)
        #take voice input from the microphone
        audio = r.listen(source)
        try:
            frase = r.recognize_google(audio)
            print(f"Did you just say: {frase} ?")
            
        except TimeoutException as msg:
            print(msg)
        except WaitTimeoutError:
            print("listening timed out while waiting for phrase to start")
            quit()
        # speech is unintelligible
        except LookupError:
            print("Could not understand what you've requested.")
        else:
            print("Your results will appear in the default browser. Good bye for now...")

    return frase

#//////////////////////////////////////////////////////////////////////////////////////////////
# Si tienes varias cámaras puedes acceder a ellas en 1, 2, etcétera (en lugar de 0)
camara = cv2.VideoCapture(0)

# https://flask.palletsprojects.com/en/1.1.x/patterns/streaming/

# Una función generadora para stremear la cámara
def generador_frames():
    while logic.bandera_camara == 0:
        ok, imagen = obtener_frame_camara()
        if not ok:
            break
        else:
            # Regresar la imagen en modo de respuesta HTTP
            yield b"--frame\r\nContent-Type: image/jpeg\r\n\r\n" + imagen + b"\r\n"

def obtener_frame_camara():
    ok, frame = camara.read()
    if not ok:
        return False, None
    # Codificar la imagen como JPG
    _, bufer = cv2.imencode(".jpg", frame)
    imagen = bufer.tobytes()
    return True, imagen

# Cuando visiten la ruta
@app.route("/streaming_camara")
def streaming_camara():
    if logic.bandera_camara == 0:
        return Response(generador_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')
    elif logic.bandera_camara == 1:
        return Response()

# Cuando toman la foto la descarga pero con el navegador
@app.route("/tomar_foto_descargar")
def descargar_foto():
    ok, frame = obtener_frame_camara()
    if not ok:
        print(500)
      #  abort(500)
      
        return
    respuesta = Response(frame)
    respuesta.headers["Content-Type"] = "image/jpeg"
    respuesta.headers["Content-Transfer-Encoding"] = "Binary"
    respuesta.headers["Content-Disposition"] = "attachment; filename=\"foto.jpg\""
    return respuesta
    
#Cuando toma la foto y la guarda en la carpeta del proyecto
@app.route("/tomar_foto_guardar")
def guardar_foto():

    logic.bandera_camara = 1

    nombre_foto = str(uuid.uuid4()) + ".jpg"
    ok, frame = camara.read()
    if ok:
        cv2.imwrite("./static/people_photo/foto.png", frame)
        emotion()

    return jsonify({
        "ok": ok,
        "nombre_foto": nombre_foto,
    })
    
"""
    API FLASK
"""
#Graba el audio
@app.route('/record', methods=['POST'])
def get_record():
    logic.texto_input = buttonVoz()
    logic.getModelo()
    #logic.askQuestionModel()

    #Obj que va al HTML
    param = {'name':"Human",
            'forward_message':logic.texto_input,
            'user_image':logic.user_image,
            'flag':logic.is_it_finish,
            'caja_respuesta':logic.caja_respuesta,
            'caja_pregunta':logic.caja_pregunta}

    return render_template('index2.html', **param);

#Fragmentacion de porcentajer de la comida
@app.route('/fragmentacion', methods=['POST'])
def get_fragmentacion():
    img= request.form['image']
    
    classImg.loadImg(img)

    x = classImg.getListPrice(classImg.getPorcent(classImg.getAperace(classImg.pred)))

    result = classImg.getTotal(x)

    z = classImg.foodCost(result[0], result[1], result[2], result[3], result[4])

    classImg.txtFinal=classImg.foodCostList(result[0], result[1], result[2], result[3], result[4])

    print(classImg.txtFinal)
    
    #logic.user_image = os.path.join(app.config['UPLOAD_FOLDER'], img)

    param = {'name':"Human",
            'forward_message':'TEC Food',
            'user_image':logic.user_image,
            'flag':logic.is_it_finish,
            'caja_respuesta':z,
            'caja_pregunta':'The result of the Fragmentation is:'}

    return render_template('index2.html', **param);

#Pasar de prediccion.
@app.route('/predicciones', methods=['POST'])
def change_prediccion():
    #Pasa cada vez que se termina una prediccion.
    if logic.contador != -1:
        #cambio la pregunta de la prediccion a la siguietne, si no hay mas mando mensaje que se acabo y no lo dejo hacer mas.
        if logic.contador >= len(logic.diccionario_predicciones):
            logic.texto_input = logic.diccionario_predicciones[0]
            logic.contador = -1
            logic.caja_respuesta = "No more prediction."
        else:
            logic.texto_input = logic.diccionario_predicciones[logic.contador]
        logic.contador += 1

        logic.getModelo()
        logic.askQuestionModel()

    #Obj que va al HTML
    param = {'name':"Human",
            'forward_message':logic.texto_input,
            'user_image':logic.user_image,
            'flag':logic.is_it_finish,
            'caja_respuesta':logic.caja_respuesta,
            'caja_pregunta':logic.caja_pregunta}

    return render_template('index2.html', **param);

@app.route('/foto', methods=['POST'])
def show_foto():
    logic.user_image = os.path.join(app.config['UPLOAD_FOLDER'], 'foto.png')

    #Obj que va al HTML
    param = {'name':"Jarvis",
            'forward_message':logic.texto_input,
            'user_image':logic.user_image,
            'flag':logic.is_it_finish,
            'caja_respuesta':logic.caja_respuesta,
            'caja_pregunta':logic.caja_pregunta}

    return render_template('index2.html', **param);

# Enrutamiento inicial.
@app.route('/')
def show_index():

    logic.user_image = os.path.join(app.config['UPLOAD_FOLDER'], 'ironman.png')

    param = {'name':"Jarvis",
             'user_image':logic.user_image}

    return render_template("index2.html", **param)

# Comando de voz para decir la prediccion
# Enrutamiento global, cada vez que apredo el boton.
@app.route("/", methods=['POST'])
def move_forward():
    #Caso inicial.
    #if logic.contador == 0:
    #    logic.texto_input = logic.diccionario_predicciones[0]

    #Se escoge modelo a usarse
    # if logic.name_modelo == "none" or logic.is_it_finish == 1:
    #     logic.getModelo()

    # else:

    if logic.contador != -1:

        #Obtengo la respuesta de la pregunta
        text = request.form['u']
        logic.caja_respuesta = text

        #Construyo la respuesta
        logic.resultado_input.append(text) 

        #Logica para mostrar preguntas.
        logic.askQuestionModel()


    #Nueva foto que se pega en interfaz.
    #logic.user_image = os.path.join(app.config['UPLOAD_FOLDER'], 'foto.png')

    #Obj que va al HTML
    param = {'name':"Humano",
            'forward_message':logic.texto_input,
            'user_image':logic.user_image,
            'flag':logic.is_it_finish,
            'caja_respuesta':logic.caja_respuesta,
            'caja_pregunta':logic.caja_pregunta}

    return render_template('index2.html', **param)
#
#   Main
#
if __name__ == "__main__":
    app.run()

    

#
#
#
#python3 -m flask run
#
#
#

    #
    #TEST
    #

    #Automovil
    #logic.resultado_input = [1,60000,1,0,796.0,37.0,4.0]

    #Vino
    #logic.resultado_input = [7.0, 0.270, 0.36, 20.7, 0.045, 45.0, 170.0, 1.00100, 3.00, 0.450000, 8.8, 1]
    #logic.resultado_input = [5.9, 0.550, 0.10, 2.2, 0.062, 39.0, 51.0, 0.99512, 3.52, 0.531215, 11.2, 0]

    #Bitcoin
    #logic.resultado_input = ["20150607"]

    #Prestamo Cliente
    #logic.resultado_input = [158364,1337374,52,20,"single","rented","yes","Army_officer","Morena","Madhya_Pradesh",9,12]






    #
    # Logica del boton de voz
    #
    #if logic.bandera_voz == 0:
        
        #logic.bandera_voz = 1

    #logic.texto_input = buttonVoz()

        # while logic.texto_input == "none":
        #     logic.texto_input = buttonVoz()













# # Comando de voz para decir la prediccion
# # Enrutamiento global, cada vez que apredo el boton.
# @app.route("/", methods=['POST'])
# def move_forward():
#     #
#     # Logica del boton de voz
#     #
#     #if logic.bandera_voz == 0:
        
#         #logic.bandera_voz = 1

#     #logic.texto_input = buttonVoz()

#         # while logic.texto_input == "none":
#         #     logic.texto_input = buttonVoz()

#         #
#         #TEST 
#         #
#     #print(logic.texto_input)
#     #LISTO MODELO AUTOMOVILES
#     #logic.texto_input = "Predict the price of a car"

#     #LISTO MODELO VINO
#     #logic.texto_input = "Predict the price of the vine"

#     #MoldeloAsociacionProductos
#     #logic.texto_input = "Predict what association these products are"

#     #MODELO BITCOIN
#     #logic.texto_input = "Predict the price of the bitcoin"

#     #MODELO PRESTAMO CLIENTE
#     #logic.texto_input = "Predict if a customer will be given a loan"
    
#     #MODELO ZAPATOS
#     #logic.texto_input = "Predict how good the model is to choose to classify shoes"

#     #MODELO OJOS
#     #logic.texto_input = "Predict how good the model is at predicting eye classification"

#     #MODELO AGUACATE
#     #logic.texto_input = "Predict the price of avocado"

#     #Caso inicial.
#     if logic.contador == 0:
#         logic.texto_input = logic.diccionario_predicciones[0]

#     #Se escoge modelo a usarse
#     if logic.name_modelo == "none" or logic.is_it_finish == 1:
#         logic.getModelo()

#     else:
#         #Obtengo la respuesta de la pregunta
#         text = request.form['u']
#         logic.caja_respuesta = text

#         #Construyo la respuesta
#         logic.resultado_input.append(text) 

#     #
#     #TEST
#     #

#     #Automovil
#     #logic.resultado_input = [1,60000,1,0,796.0,37.0,4.0]

#     #Vino
#     #logic.resultado_input = [7.0, 0.270, 0.36, 20.7, 0.045, 45.0, 170.0, 1.00100, 3.00, 0.450000, 8.8, 1]
#     #logic.resultado_input = [5.9, 0.550, 0.10, 2.2, 0.062, 39.0, 51.0, 0.99512, 3.52, 0.531215, 11.2, 0]

#     #Bitcoin
#     #logic.resultado_input = ["20150607"]

#     #Prestamo Cliente
#     #logic.resultado_input = [158364,1337374,52,20,"single","rented","yes","Army_officer","Morena","Madhya_Pradesh",9,12]

#     #
#     #
#     #logic.resultado_input = [0]

#     #Logica para mostrar preguntas.
#     logic.askQuestionModel()

#     #Pasa cada vez que se termina una prediccion.
#     if logic.contador == -1:
#         logic.caja_respuesta = "Ya no hay mas predicciones."

#     #Nueva foto que se pega en interfaz.
#     logic.user_image = os.path.join(app.config['UPLOAD_FOLDER'], 'foto.png')

#     #Obj que va al HTML
#     param = {'name':"Humano",
#             'forward_message':logic.texto_input,
#             'user_image':logic.user_image,
#             'flag':logic.is_it_finish,
#             'caja_respuesta':logic.caja_respuesta,
#             'caja_pregunta':logic.caja_pregunta}

#     return render_template('index2.html', **param);