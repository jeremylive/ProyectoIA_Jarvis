from PIL import Image, ImageDraw
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials

# This key will serve all examples in this document.
API_KEY = "488b275b0ed64cf8b34e79fcf6069b3a"
# This endpoint will be used in all examples in this quickstart.
ENDPOINT = "https://faceapiazureia.cognitiveservices.azure.com"

face_client = FaceClient(ENDPOINT, CognitiveServicesCredentials(API_KEY))

#img_file = open('foto.png','rb')
img_file = open('../static/people_photo/foto.png','rb')

responce_detection = face_client.face.detect_with_stream(
    image = img_file,
    detection_model= 'detection_01',
    recognition_model='recognition_04',
    return_face_attributes=['age','emotion'],
)
img = Image.open(img_file)
draw = ImageDraw.Draw(img)
#font = ImageFont.truetype('C:\Windows\Fonts\OpenSans-Bold.ttf',25)
for face in responce_detection:
    age = face.face_attributes.age
    emotion = face.face_attributes.emotion
    neutral = '{0:0f}%'. format(emotion.neutral * 100)
    happiness = '{0:0f}%'. format(emotion.happiness * 100)
    anger = '{0:0f}%'. format(emotion.anger * 100)
    sadness = '{0:0f}%'. format(emotion.sadness * 100)

    rect = face.face_rectangle
    left = rect.left
    top = rect.top
    right = rect.width + left
    botton = rect.height + top 
    draw.rectangle(((left, top),(right,botton)), outline = 'green', width=5)

    draw.text((right + 4,top), 'Age: '+str(int(age)),fill=(255,255,255))
    draw.text((right + 4,top+35), 'Neutral: '+neutral,fill=(255,255,255))
    draw.text((right + 4,top+70), 'Happy: '+happiness,fill=(255,255,255))
    draw.text((right + 4,top+105), 'Sad: '+sadness,fill=(255,255,255))
    draw.text((right + 4,top+140), 'Angry: '+anger,fill=(255,255,255))

    img.save("../static/people_photo/foto.png")

    break
img.show()